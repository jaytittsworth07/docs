## Overview

GitLab auto-deploy deployments allow us to increase the frequency
of deployments to GitLab.com from the master branch of
[gitlab-ee](https://gitlab.com/gitlab-org/gitlab-ee)
and [omnibus-gitlab](https://gitlab.com/gitlab-org/omnibus-gitlab).

This process is a shift from the monthly release cycle that is currently in place, and is generally a disruptive workflow change.
Below you can find details of the transition plan as well as a basic process overview with answers to some frequently asked questions.

## Transition plan

[Transition plan](auto-deploy-transition.md) contains overview per stakeholder in the process as well as a transition schedule.

## Process overview
### Auto-deploy branches

Approximately once a week, up until the release date on the 22nd, auto-deploy
branches are created with the following format:

```
<MAJOR>-<MINOR>-auto-deploy-<ID>
```

example: `11-10-auto-deploy-0000001`


* `MAJOR.MINOR`: This comes from the currently active milestone on the gitlab-ce
  project. It generally tracks to the release month and has the following
  requirements:
  * It must be an _active_ milestone
  * The start date must be in the _past_
  * The end date must be in the _future_
* `ID`: The pipeline `$CI_PIPELINE_IID` of the release-tools job that
  created the branch. This ID is guaranteed to increment on every auto-deploy
  branch.

Auto-deploy branches are created for the following projects:

* gitlab-ee
* gitlab-ce
* omnibus-gitlab


These branches are created from the latest commit on the `master` branch at
the moment of branch creation.
As soon as these branches are created, the latest green (passing) commit on the branch
will be tagged for an auto-deployment. This means that the HEAD of the auto-deploy
branch won't necessarily be the one that is a candidate for deployment. This is done to
ensure that we always deploy commits with some confidence.

When omnibus-gitlab is tagged, a new EE omnibus-gitlab package is created and a
deployment to a non-production environment is executed. At that point in time,
deployment to a production environment is done only if no issues were found on
the non-production environment.

Auto-deploy branches are _protected branches_, meaning that they require special
permission for merging and pushing, and are also automatically mirrored to
dev.gitlab.org. The permissions for merging and pushing are only granted to the
release managers group.

### Auto-deploy tagging

For every deployment created using the auto-deploy process, there is a git tag
that matches the version of what is deployed. The auto-deploy tag has the following format:

```
<MAJOR>.<MINOR>.<TIMESTAMP>+<gitlab-ee sha>.<omnibus-gitlab sha>
```


* `MAJOR.MINOR`: This is the currently active milestone in the `gitlab-org`
  group on gitlab.com and follows the same requirements
  for the auto-deploy branch (see above).
* `TIMESTAMP`: Timestamp of the commit in omnibus-gitlab project. This value is guaranteed to increment when a new commit is made in omnibus-gitlab, which will happen at minimum when something changes the GitLab Rails project.
* `gitlab-ee sha`: The sha of gitlab-ee for auto-deploy, it corresponds to a
  green (passing) commit on the gitlab-ee auto-deploy branch.
* `omnibus-gitlab sha`: The sha of omnibus-gitlab that will be used for the next
  auto-deploy, it corresponds to a green (passing) ref on the omnibus-gitlab
  auto-deploy branch

As mentioned previously, the omnibus-gitlab tag will trigger a deployment to a
non-production environment.

### Auto-deploy schedule

Using an example of a non existent release **11.12**, let's assume that a
milestone is created with a start date of the 23rd and a due date of the 22nd.

The table below gives an overview of the schedule:

<table>
<thead>
<tr>
<th>Day</th>
<th>Description</th>
</tr>
</thead>

<tr>
  <td>23rd</td>
  <td>

  * The **11.12** milestone is active.
  * An automated CI job creates a branch named **11-12-auto-deploy-00001** in gitlab-ce, omnibus-gitlab and
    gitlab-ee from the **master** branch.
    * The latest green commit of gitlab-ee is used to update versions of GitLab
      services in the omnibus-gitlab repository.
    * The commits in gitlab-ee and omnibus-gitlab are tagged with **11.12.201907021014+aaaa.ffff**.
    * The resulting auto-deploy package is deployed to a non-production environment.

  </td>
</tr>
<tr>
  <td>23rd..1st</td>
  <td>

  * In case an issue is discovered after deployment, one or many MRs need to be created resolving the issue.
  * MRs are reviewed and merged to the `master` branch using the regular process.
  * In order for the MR to be included in auto-deploy branch, a label **Pick into auto-deploy** needs to be applied.
  * A CI job checks the MRs for auto-deploy
    * Labeled MRs are picked automatically into the **11-12-auto-deploy-00001** branch
    * The merge train ensures that CE is merged into EE
    * MRs are updated with a comment to let the MR author know that the change has been prepped for next deployment.
  * The next green commit on the auto-deploy branch is tagged with **11.12.201907021014+bbbb.ffff**
    * The resulting auto-deploy package is deployed to the non-production environment.
    * In case the latest green commit has already been deployed, no action is taken.
  * This process is repeated on a frequent interval, with multiple deployments
    to non-production environments.
  * Other MRs (ones without the label) merged to the `master` branch are not included until the next week's
    auto-deploy branch, **11-12-auto-deploy-00002**

  </td>
</tr>
<tr>
  <td>1st</td>
  <td>

  * Branch **11-12-auto-deploy-00002** created in gitlab-ce, omnibus-gitlab and
    gitlab-ee from the **master** branch.
  * The same automated process runs as explained above.
  * The same picking and deploying process is now done for the new auto-deploy
    branch, **11-12-auto-deploy-00002**
  </td>
</tr>
<tr>
  <td>8th</td>
  <td>

  * Branch **11-12-auto-deploy-00003** created in gitlab-ce, omnibus-gitlab and
    gitlab-ee from **master**
  * The same picking and deploying process is now done for the new auto-deploy
    branch, **11-12-auto-deploy-00003**

  </td>
</tr>
<tr>
  <td>15th</td>
  <td>

  * Branch **11-12-auto-deploy-00004** created in gitlab-ce, omnibus-gitlab and
    gitlab-ee from **master**
  * The same picking and deploying process is now done for the new auto-deploy
    branch, **11-12-auto-deploy-00004**

  </td>
</tr>
<tr>
  <td>22nd RELEASE DAY</td>
  <td>

  * The commit that is currently deployed to production will be used for the official
    release.
  * Changes that are released on production will be part of the release blog
    post.
  * If the MR introducing a change is picked into **11-12-auto-deploy-00004** but not
    deployed to production, the change in question is not going to be included into the final
    self-managed release.
  * 11.12 is published.

  </td>
</tr>

<tr>
  <td>23rd</td>
  <td>

  * A new milestone, **11.13** is active with a start date of the 23rd and a due date of the 22nd
  * The process auto-deploy cycle repeats

  </td>
</tr>

</table>

## FAQ

### How does this change the existing feature freeze on the 7th?

Throughout the growth of GitLab as a company, the feature freeze implied
creation of a `stable` branch which also meant that any fix would have to wait for the
next cycle, sometimes a full month after the initial release of a feature.

From the perspective of the release, the feature freeze becomes obsolete. Any change
introduced to the `master` branch will be deployed through the environments, and
will find its way to GitLab.com within a week (at least at the beginning)
after it has been merged.

### How often do we auto-deploy?

The automated pipelines are running on a 3 hour schedule and are automatically targeting the first available non-production environment (In most cases this would be the staging environment). Since the deployment pipeline only looks for a green (passing) CI build, it is important that any change of interest passes CI tests in the auto-deploy branch. Deployment to the production canary environment is done attended at minimum once a week, beginning from Wednesday's. GitLab.com deployments are going to be also attended based on the previous weeks successful canary deployment.

Example:
From the moment the automated cherry-picker leaves a message that the change is picked,
the CI pipeline will run for that specific commit in the branch. Depending on the time the
specs passed, the change might be deployed to the first non-production environment anywhere
from 3-6 hours.

*Note*: The frequency of the auto-deployment is set conservatively at the beginning while we measure the impact on environment stability as well as process transition. As we get more comfortable, the goal is to have even more frequent deployment to production environments.

### What about registry, workhorse, gitaly, pages and other components?

All GitLab created components such as gitlab-workhorse, gitaly, gitlab-pages and similar
have a `VERSION` file inside of the GitLab EE repository. For the time being, this remains unchanged and these files are the source of truth for the deployment pipelines.

Components such as Registry have the version set inside of the omnibus-gitlab project,
and that is the source of truth for the deployment pipelines for the time being.

### What will happen with security patches? 

Process for P1/S1 security patches remains the same. 
Security patches included in the regular monthly security release will be deployed to GitLab.com prior to publishing by merging the changes into the `master` branch, and manually including them in the active auto-deploy branch. 
This change will only affect release managers, developers and other process stakeholders won't have a process change for the time being.  

### How do I mark an MR so that will be picked into the auto-deploy branch?

In most cases, you won't need to apply an additional label to your MR because your change will be deployed the following Monday after the MR has been merged.
In cases where a bug has been found and the priority and severity are the highest,
you will need to apply the `Pick into auto-deploy` label. This will ensure that the automated cherry-picker picks up the change and prep it for the first next auto-deployment.

In cases where the MR was not able to pick cleanly to the auto-deploy branch,
you will need to create a MR targeting the currently active auto-deploy branch and assign to all active release managers. The auto-deploy branch name will be posted in the comment
created by the cherry-pick bot.

### Does auto-deploy change how we release software to the self-managed users?

Yes and no.
The stable branches are created on the 22nd instead of the 7th, and the stable branches
become backport branches for any patch releases that needs to happen in future.
The self-managed users will still receive the next release on the 22nd
of the month and will receive patch releases as needed, as was the case up until now.

### How are auto-deploy branches different than the stable branch?

The auto-deploy branches are shorter lived and are created from the `master` branch every
week. You can consider the auto-deploy branches more of a "slowed down" `master` branch
than a long running `stable` branch. The auto-deploy branches are intended to have a weekly lifespan.

### Why are we not deploying directly from the master branch?

The `master` branch receives a large number of changes daily. While we do have a large
test coverage covering individual pieces, end to end test coverage is still being improved.
We are also still lacking the large enough data set for developers to test their changes at scale
and number of issues are still discovered at a non-production environment, and in some cases in production too.
By the time an issue is discovered, the `master` branch received hundreds of different changes and it is pretty
difficult to isolate the problem without introducing another one that would block further deployments.
Blocking deployments has a worse effect; once the deployments are enabled again we would be introducing an even larger data set. The auto-deploy branches are giving us some time to discover issues and fix them without the pressure.

### How do I query what ref is running on gitlab.com or a non-production environment?

On GitLab.com you can check the `/help` page when logged in and see the sha of deployed commit.
We will be adding methods of exposing the versions as we get the process in place.

### How will marketing teams know what features to put in the release blog post?

The release-post managers and product management will have to change the way the release blog post is created.
The release blog post will have to be composed of items that are deployed on GitLab.com as opposed to what was planned for delivery. Release on the 22nd is the snapshot of what is active on GitLab.com.
