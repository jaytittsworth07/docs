# Security Releases (Critical / Non-critical) as a Developer

The release deadlines for a critical or non-critical security release are different.
Check the [Release deadlines](process.md) first to know when the security
fixes have to be merged by.

## DO NOT PUSH TO GITLAB.COM!

As a developer working on a fix for a security vulnerability, your main concern
is not disclosing the vulnerability or the fix before we're ready to publicly
disclose it.

To that end, you'll need to be sure that all of the development for a fix
happens on [dev.gitlab.org], because it's not publicly-accessible.

[dev.gitlab.org]: https://dev.gitlab.org/

## Process

As with most GitLab development, a security fix starts with an issue identifying
the vulnerability. In this case, it should be a confidential issue on
[gitlab.com].

Once a security issue is assigned to a developer, we follow the same merge
request and code review process as any other change, but on [dev.gitlab.org].
All security fixes are released for [at least three monthly releases], and you
will be responsible for creating backports as well.

When working on a high severity ~S1 issue, the initial work on a [post-deployment patch](#creating-a-post-deployment-patch) as described below, may be done before any
other tasks in the normal workflow at the request of the security engineer, with
the review of the delivery team and infrastructure team as outlined in the
post-deployment patch process.

[gitlab.com]: https://gitlab.com/

### Preparation

- Before starting, run `scripts/security-harness` in the CE/EE repo you will implement
  the fix in. This script will install a Git `pre-push` hook that will prevent
  pushing to any remote besides `dev.gitlab.org`, in order to prevent accidental
  disclosure.
- [Create a new issue on org](https://dev.gitlab.org/gitlab/gitlabhq/issues/new?issuable_template=Security+developer+workflow) using the [Security Developer Workflow] template.
- Security vulnerabilities that exist in **both** CE and EE should be fixed in
  the [CE project on org](https://dev.gitlab.org/gitlab/gitlabhq), and a corresponding MR is required for EE in order to avoid unexpected conflicts and failing tests.
- Security vulnerabilities that exist only in EE should be fixed in the [EE
  project on org](https://dev.gitlab.org/gitlab/gitlab-ee).
- Security vulnerabilities that exist in Omnibus should be fixed in the [Omnibus
  project on org](https://dev.gitlab.org/gitlab/omnibus-gitlab).

[security developer workflow]: https://dev.gitlab.org/gitlab/gitlabhq/blob/master/.gitlab/issue_templates/Security%20developer%20workflow.md

### Branches

Because all security fixes go into [at least three monthly releases], you'll be
creating at least three branches for your fix. Merge requests for previously
released versions should target the appropriate stable branches. For example, a
fix for version 11.2.3 should target the branch `stable-11-2`. In the past we
used to target dedicated security branches, but this is no longer the case.

Your branch name must start with `security`, such as: `security-rs-milestone-xss-10-6`.

Branch names that start with `security` will be rejected upon a push from the CE and EE
repositories on GitLab.com. This helps ensure security merge requests don't get
leaked prematurely.

### Development

#### Create an issue

On `dev.gitlab.org`, create an issue and select `Security developer workflow` on the template dropdown. The title should be the same as the original one created on `gitlab.com`, for example: `Prevent stored XSS in code blocks`.

This issue is now your "Implementation issue" and a single source of truth for
all related issues and merge requests. Once the issue is created, assign it to yourself and start working on the tasks.

#### Create merge requests

Open a merge request in the relevant [dev.gitlab.org] project and use the `Security Release` merge request template. 
Target the `stable-X-Y` branch that belongs to the target version. 
Change the milestone to `X-Y` (e.g. a `stable-11-10` backport MR would have `11.10` set as its milestone).

Once your merge requests are created, you should update the security developer workflow issue to include links to all of them.

**IMPORTANT** All MRs created need to have green pipelines, correct labels and milestones set. In case one of these items is missing or incorrect, release managers will re-assign all related merge requests to the original author and remove the issue from the current security release.
This is necessary due to the number of merge requests that need to be handled for each security release. MRs are to be merged by RMs only.

#### Creating a post-deployment patch

For high severity ~S1 issues, a post-deployment patch may be required.

If a post-deployment patch was not already created as part of the initial response
to an ~S1 issue, verify with the security engineer if one is required. If an
existing patch exists, work with the security engineer to determine if the
temporary patch should be replaced with one based on the completed solution.

Follow the [post-deployment patch process](../deploy/post-deployment-patches.md).

### `secpick` script

This is a small script that helps cherry-picking across multiple releases. It will stop
if there is a conflict while cherry-picking, otherwise will push the change to `org`.

The list of options available running:

```
$ bin/secpick --help
```

For example:

```
bin/secpick -v 10.6 -b security-fix-mr-issue -s SHA
```

It will change local branches to push to a new security branch for each specified release,
meaning that local changes should be saved prior to running the script.

This is only useful if we have squashed the original MR commits into a single one, easier
to cherry-pick.

### Final steps

After the Release Manager promotes the packages to public, your fix will need to go into into the `master`
branch to ensure that it's included in all future releases. The release manager will merge
the MR, so make sure all the MRs, including the MR targeting the master branch have the
`security` label.

Be sure to run `scripts/security-harness` again to enable pushing to remotes
other than `dev.gitlab.org`!

### Questions?

If you have any doubts or questions, feel free to ask for help in the #development
channel in Slack.

---

[Return to Security Guide](process.md)

[at least three monthly releases]: https://docs.gitlab.com/ee/policy/maintenance.html#security-releases
